import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ActionsClass {

	public static void main(String[] args) throws InterruptedException {	
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
//		driver.get("https://www.flipkart.com/");
		
		Actions act = new Actions(driver);
//		WebElement we = driver.findElement(By.xpath("//img[@alt='Fashion']"));
//		Thread.sleep(5000);
//		act.moveToElement(we).build().perform();
		
//		act.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ent)
		
		
		driver.get("https://jqueryui.com/droppable/");
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
		
		WebElement src= driver.findElement(By.xpath("//p[text()='Drag me to my target']"));
		WebElement target = driver.findElement(By.xpath("//p[text()='Drop here']"));
		act.moveToElement(src).dragAndDrop(src, target).build().perform();
		act.release();
	
		
	}

}
