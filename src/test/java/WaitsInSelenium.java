import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitsInSelenium {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		// Implicit Wait
		// Explicit Wait
//		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.MILLISECONDS);

		WebDriverWait wait = new WebDriverWait(driver, 45);

		driver.findElement(By.id("user-message")).sendKeys("asdf");

		WebElement we1 = wait.until(ExpectedConditions
				.presenceOfElementLocated((By.xpath("//button[text()='Show Message1']"))));
		we1.click();
		
		
		
	}

}
