import java.io.File;
import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.io.FileUtils;
public class ScreenShotInJava {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		
		TakesScreenshot tsa = (TakesScreenshot)driver;
		File srcFile = tsa.getScreenshotAs(OutputType.FILE);
		File desFle = new File("C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\rj.jpeg");
        FileUtils.copyFile(srcFile, desFle);
	}
}
