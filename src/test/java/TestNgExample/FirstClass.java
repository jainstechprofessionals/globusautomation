package TestNgExample;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class FirstClass {
	@Test(description = "This test case is to verify the login functionality")
	public void login() {
		System.out.println("in  Login");
		Assert.assertEquals(false, true);

		System.out.println("in login after fail");
	}

//  @Test(invocationCount = 5)
//  public void tc_001() {
//	  System.out.println("in tc001");
//  }
	
	
	@Test
	public void tc_002() {
		System.out.println("in tc_002");
	}
	

	@Test(priority = -1, dependsOnMethods = "login")
	public void home() {
		System.out.println("in home");
	}

//  @BeforeMethod
//  public void beforeMethod() {
//	  System.out.println("in beforeMethod");
//  }
//
//  @AfterMethod
//  public void afterMethod() {
//	  System.out.println("in afterMethod");
//  }
//
//  @BeforeClass
//  public void beforeClass() {
//	  System.out.println("in BeforeClass");
//  }
//
//  @AfterClass
//  public void afterClass() {
//	  System.out.println("in afterClass");
//  }
//
//  @BeforeTest
//  public void beforeTest() {
//	  System.out.println("in beforeTest");
//  }
//
//  @AfterTest
//  public void afterTest() {
//	  System.out.println("in afterTest");
//  }
//
//  @BeforeSuite
//  public void beforeSuite() {
//	  System.out.println("in beforeSuite");
//  }
//
//  @AfterSuite
//  public void afterSuite() {
//	  System.out.println("in afterSuite");
//  }

}
