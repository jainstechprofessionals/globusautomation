package TestNgExample;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Day2 {

	WebDriver driver; // instance variable

	@BeforeMethod
	public void init() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.testfire.net/login.jsp");
	}

	@Test
	public void tc_001() {
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
		driver.findElement(By.xpath("//input[@value='Login']")).click();
		String str = driver.getTitle();
		System.out.println(str);
		boolean flag;
		if (str.equals("Altoro Mutual")) {
			flag = true;
		} else {
			flag = false;
		}
		Assert.assertTrue(flag, "Test case failed: As it does not match the title");
	}

	@Test
	public void tc_002() {
		driver.findElement(By.id("uid")).sendKeys("jsmith1");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
		driver.findElement(By.xpath("//input[@value='Login']")).click();
		String str = driver.findElement(By.id("_ctl0__ctl0_Content_Main_message")).getText();
	Assert.assertTrue(str.contains("Login Failed:"));
	
	}

	@AfterMethod
	public void killSession() {
		driver.quit();
	}
}
