package BaseClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseClass {

	public static Properties prop;
	public static WebDriver driver;

	public BaseClass(){
		FileInputStream fis;
		File fi= new File("C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\config.properties");
		try {
			fis = new FileInputStream(fi);
			prop= new Properties();
			prop.load(fis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}
	
	public void initialize() {
		String bName = prop.getProperty("browserName");
		
		if(bName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if(bName.equalsIgnoreCase("ff")) {
			
		}		
		driver.manage().window().maximize();
		driver.get(prop.getProperty("url"));
		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);	
		
	}
	
	

}
