import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlingInSelenium {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/window-popup-modal-demo.html");		
		
		String str = driver.getWindowHandle();
		System.out.println(str);
//		driver.findElement(By.xpath("//a[text()='  Follow On Twitter ']")).click();
		
		driver.findElement(By.id("followall")).click();
		
		Set<String> st = driver.getWindowHandles();
		
		Iterator<String> itr = st.iterator();
		
		while(itr.hasNext()) {
			String c = itr.next();
			System.out.println(c);
			
			driver.switchTo().window(c);
			System.out.println(driver.getTitle());
			String ss = driver.getTitle();
			if(ss.contains("Twitter")) {
				driver.switchTo().window(c);
				break;
			}
			
		}
		
		System.out.println(driver.getTitle());
		
		driver.switchTo().window(str);
		System.out.println(driver.getTitle());
	}

}
