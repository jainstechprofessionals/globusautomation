package PageObjects;

import org.openqa.selenium.By;

import BaseClasses.BaseClass;

public class LoginPO extends BaseClass{

	
	By uname =By.id("uid");
	By pwd  = By.id("passw");
	By login = By.name("btnSubmit");
	By verTitle = By.xpath("//h1[contains(text(),'Hello')]");
	
	public void enterUserName(String userN) {
		driver.findElement(uname).sendKeys(userN);
	}
	
	public void enterPassword(String passW) {
		driver.findElement(pwd).sendKeys(passW);
	}
	
	public void clickLogin() {
		driver.findElement(login).click();
	}
	
	public void login(String userN,String passW) {
		enterUserName(userN);
		enterPassword(passW);
		clickLogin();
	}
	
	public boolean verifyTitle() {
		boolean b = driver.findElement(verTitle).isDisplayed();
		return b;
	}
}
