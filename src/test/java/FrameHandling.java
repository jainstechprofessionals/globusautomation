import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrameHandling {

	public static void main(String[] args) throws InterruptedException {
		
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/accordion/");
		Thread.sleep(4000);		
		WebElement we =driver.findElement(By.xpath("//iframe[@class='demo-frame']"));		
		driver.switchTo().frame(we);		
		driver.findElement(By.xpath("//h3[text()='Section 2']")).click();
		
	}

}
