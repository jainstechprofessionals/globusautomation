import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectClassExample {

	public static void main(String[] args) throws InterruptedException {
		
		
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.testfire.net/login.jsp");
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
//		driver.findElement(By.name("btnSubmit")).click();
		driver.findElement(By.xpath("//input[@value='Login']")).click();
		
		WebElement we = driver.findElement(By.id("listAccounts"));
		
		Select sel = new Select(we);
		
//		sel.selectByIndex(1);
//		Thread.sleep(4000);
//		sel.selectByValue("4539082039396288");
//		Thread.sleep(4000);
//		sel.selectByVisibleText("800002 Savings");
		
		List<WebElement>lst = sel.getOptions();
		
		for(int i=0 ;i<lst.size();i++) {
			String str = lst.get(i).getText();
			System.out.println(str);
		}
		 
	}

}
