package TestClasses;

import org.testng.annotations.Test;

import BaseClasses.BaseClass;
import PageObjects.LoginPO;

import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

public class LoginTest extends BaseClass {
	LoginPO lp;

	@BeforeMethod
	public void beforeMethod() {

		initialize();
		lp = new LoginPO();
	}

	@Test(description = "To verify if user is able to login successfully")
	public void tc_001() {
		String userN = prop.getProperty("username");
		String passW = prop.getProperty("password");
		lp.login(userN, passW);
		boolean f = lp.verifyTitle();
		Assert.assertTrue(f);
	}

	
	@Test(description = "To verify if user is able to login successfully")
	public void tc_002() {
		driver.navigate().refresh();
		driver.navigate().back();
		driver.navigate().forward();
	}
	
	
	@AfterMethod
	public void afterMethod() {
	}

}
