import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JavaScriptExecutorPractice {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");		
		
		driver.findElement(By.id("user-message")).sendKeys("asdf");
		
		WebElement we =driver.findElement(By.xpath("//button[text()='Show Message']"));
      	
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		js.executeScript("arguments[0].click();", we );
		we =driver.findElement(By.xpath("//button[text()='Show Message']"));
		js.executeScript("arguments[0].style.border='2px solid red'", we); 
	
	
	}

}
