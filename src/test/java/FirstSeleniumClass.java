import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstSeleniumClass {

	public static void main(String[] args) {
		// find the chrome
		// Launch the browser
		// Enter the url
		// Enter username
		// Enter Password
		// Click on Login
		// Verify user is able to login
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\dell\\eclipse-workspace\\GlobusAutomation\\src\\test\\resources\\chromedriver.exe");
		WebDriver driver;
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://demo.testfire.net/login.jsp");
		driver.findElement(By.id("uid")).sendKeys("jsmith");
		driver.findElement(By.id("passw")).sendKeys("demo1234");
//		driver.findElement(By.name("btnSubmit")).click();
		driver.findElement(By.xpath("//input[@value='Login']")).click();

		// Xpath
//		1. Absolute (Static)
		
//		2. Relative (Dynamic)
//		a) Basic xpath : //input[@value='Login']
//		b) text() :  //a[text()='View Recent Transactions']
//		c) contains : //h1[contains(text(),'Hello')]
		//a[contains(@id,'MenuHyper')]		
//		d) Starts-with : //h1[starts-with(text(),'Hello')]
//		e) //a[text()='Privacy Policy']/..
		
//		f) //a[text()='Privacy Policy']/following-sibling::a[text()='Security Statement']
//		g) //a[text()='Security Statement']/preceding-sibling::a
//		h) //div[@id='footer']/child::a
//		i) //div[@id='footer']//parent::div
//		j) //div[@id='footer']//ancestor::html
	}

}
